" Instead of writing the emojis directly in my notes, I tag/markers instead. Then I call this function to replace them
" throughout the entire file. Simply execute :ReplaceMarkers and it will do it's work.
fun! ReplaceMarkers()
    execute ":%s/:DONE:/✅/ge"
    execute ":%s/:TODO:/🚧/ge"
    execute ":%s/:REPORT:/📰/ge"
    execute ":%s/:QUESTION:/❓/ge"
endfunction


command ReplaceMarkers call ReplaceMarkers()

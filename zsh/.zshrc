# vim:ts=4:sw=4:ai:foldmethod=marker:foldlevel=0:

# causes very high start up time
# zstyle ':completion:*' use-cache on
autoload -Uz compinit
compinit

[[ -e ~/.antidote ]] || git clone https://github.com/mattmc3/antidote.git ~/.antidote
. ~/.antidote/antidote.zsh
antidote load

ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE="bold,underline"
bindkey '^ ' autosuggest-accept

bindkey '^[[A' history-substring-search-up
bindkey '^[[B' history-substring-search-down

eval "$(starship init zsh)"

#THIS MUST BE AT THE END OF THE FILE FOR SDKMAN TO WORK!!!
export SDKMAN_DIR="$HOME/.sdkman"
[[ -s "$HOME/.sdkman/bin/sdkman-init.sh" ]] && source "$HOME/.sdkman/bin/sdkman-init.sh"

export PATH="/usr/local/sbin:$PATH"

complete -o nospace -C /usr/local/bin/terraform terraform
source <(kubectl completion zsh)
eval "$(direnv hook zsh)"


